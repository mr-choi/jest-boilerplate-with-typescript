import charCount from ".";

test("testcase 1", () => {
    expect(charCount("a", "edabit")).toBe(1);
});
test("testcase 2", () => {
    expect(charCount("c", "Chamber of secrets")).toBe(1);
});
test("testcase 3", () => {
    expect(charCount("B", "boxes are fun")).toBe(0);
});
test("testcase 4", () => {
    expect(charCount("b", "big fat bubble")).toBe(4);
});
test("testcase 5", () => {
    expect(charCount("e", "javascript is good")).toBe(0);
});
test("testcase 6", () => {
    expect(charCount("!", "!easy!")).toBe(2);
});