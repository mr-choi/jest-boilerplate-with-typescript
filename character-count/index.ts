export default function charCount(char:string, str:string) {
    let count = 0;
    for (const c of str) {
        if (c === char) count++
    }
    return count;
    // let result = str.match(new RegExp(char, "g"))
    // return (result) ? result.length : 0;
}