export default function splitOnDoubleLetter(word:string) {
    word = word.toLowerCase();
    
    let newword = word[0];
    for (let i=1; i<word.length; i++) {
        if (word[i] === word[i-1]) {
            newword += "-";
        }
        newword += word[i];
    }
    let result = newword.split("-");
    return (result.length === 1) ? [] : result;
}