import splitOnDoubleLetter from ".";

test("testcase 1", () => {
    expect(splitOnDoubleLetter("letter")).toEqual(["let","ter"]);
});
test("testcase 2", () => {
    expect(splitOnDoubleLetter("Really")).toEqual(["real","ly"]);
});
test("testcase 3", () => {
    expect(splitOnDoubleLetter("Happy")).toEqual(["hap","py"]);
});
test("testcase 4", () => {
    expect(splitOnDoubleLetter("Shall")).toEqual(["shal","l"]);
});
test("testcase 5", () => {
    expect(splitOnDoubleLetter("Tool")).toEqual(["to","ol"]);
});
test("testcase 6", () => {
    expect(splitOnDoubleLetter("Mississippi")).toEqual(["mis","sis","sip","pi"]);
});
test("testcase 7", () => {
    expect(splitOnDoubleLetter("Easy")).toEqual([]);
});
test("Identical letters with first capital", () => {
    expect(splitOnDoubleLetter("Aaaahh")).toEqual(["a","a","a","ah","h"]);
});