type TaxData = {
    name: string;
    surname: string;
    gender: string;
    dob: string;
}

const months = { 1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "H", 7: 
"L", 8: "M", 9: "P", 10: "R", 11: "S", 12: "T" };

export default function fiscalCode(data: TaxData) {
    // The surname
    let surnameCode : string;
    const surnameCon = data.surname.toUpperCase().match(/[B-DF-HJ-NP-TV-Z]/g) || [];
    const surnameVow = data.surname.toUpperCase().match(/[AEIOU]/g) || [];
    
    if (data.surname.length >= 3) {
        const together = [...surnameCon, ...surnameVow];
        surnameCode = together.slice(0,3).join("");
    } else {
        surnameCode = surnameCon[0] + surnameVow[0] + "X";
    }
    // The first name
    let nameCode : string;
    const nameCon = data.name.toUpperCase().match(/[B-DF-HJ-NP-TV-Z]/g) || [];
    const nameVow = data.name.toUpperCase().match(/[AEIOU]/g) || [];

    if (data.name.length >= 3) {
        const together = [...nameCon, ...nameVow];

        if (nameCon.length > 3) {
            together.splice(1,1);
        }

        nameCode = together.slice(0,3).join("");
    } else {
        nameCode = nameCon[0] + nameVow[0] + "X";
    }

    // The rest
    let finalPart : string
    const [day, month, year] = data.dob.split("/");
    const yearCode = year.slice(2,4);
    const monthCode = months[month];
    let dayCode : string;
    if (data.gender === "M") {
        dayCode = (parseInt(day) < 10) ? "0" + day : day;
    } else {
        dayCode = (parseInt(day) + 40).toString();
    }
    finalPart = yearCode + monthCode + dayCode;
    return [surnameCode, nameCode, finalPart].join("");
}