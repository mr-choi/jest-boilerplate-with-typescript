import fiscalCode from ".";

test("testcase 1", () => {
    expect(fiscalCode({ 
        name: "Matt", 
        surname: "Edabit", 
        gender: "M", 
        dob: "1/1/1900" 
    })).toBe("DBTMTT00A01");
});
test("testcase 2", () => {
    expect(fiscalCode({ 
        name: "Helen", 
        surname: "Yu", 
        gender: "F", 
        dob: "1/12/1950" 
    })).toBe("YUXHLN50T41");
});
test("testcase 3", () => {
    expect(fiscalCode({ 
        name: "Mickey", 
        surname: "Mouse", 
        gender: "M", 
        dob: "16/1/1928" 
    })).toBe("MSOMKY28A16");
});