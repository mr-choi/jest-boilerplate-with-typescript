export default function elasticWord(word:string) {
    // Init:
    const len = word.length;
    const arr = [];

    // Stretch the letters:
    for (let i=0; i<len; i++) {
        let char = word[i];
        let stretch = Math.min(i+1, len-i);
        arr.push(char.repeat(stretch));
    }
    return arr.join(""); // Join together.
}