import elasticWord from "."

test("testcase 1", () => {
    expect(elasticWord("ANNA")).toBe("ANNNNA");
})
test("testcase 2", () => {
    expect(elasticWord("KAYAK")).toBe("KAAYYYAAK");
})
test("testcase 3", () => {
    expect(elasticWord("X")).toBe("X");
})
test("custom testcase 1", () => {
    expect(elasticWord("abcdef")).toBe("abbcccdddeef");
})
test("custom testcase 2", () => {
    expect(elasticWord("#8-Ab]_")).toBe("#88---AAAAbbb]]_");
})
test("custom testcase 3", () => {
    expect(elasticWord("ab")).toBe("ab");
})