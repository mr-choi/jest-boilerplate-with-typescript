export default function passwordStrength(pwd:string) {
    // First check if password is valid
    if (pwd.search(/\s/) >= 0 || pwd.length < 6) return "Invalid";

    // Now count how much criteria is met
    let criteriaMet = 0;
    if (pwd.search(/[a-z]/) >= 0) criteriaMet++;
    if (pwd.search(/[A-Z]/) >= 0) criteriaMet++;
    if (pwd.search(/\d/) >= 0) criteriaMet++;
    if (pwd.search(/\W/) >= 0) criteriaMet++;
    if (pwd.length >= 8) criteriaMet++;

    // Based on how much criteria met, return strength
    if (criteriaMet < 3) return "Weak";
    if (criteriaMet < 5) return "Moderate";
    return "Strong";
}