import passwordStrength from ".";

test("testcase 1", () => {
    expect(passwordStrength("stonk")).toBe("Invalid");
});
test("testcase 2", () => {
    expect(passwordStrength("pass word")).toBe("Invalid");
});
test("testcase 3", () => {
    expect(passwordStrength("password")).toBe("Weak");
});
test("testcase 4", () => {
    expect(passwordStrength("1108992")).toBe("Weak");
});
test("testcase 5", () => {
    expect(passwordStrength("mySecurePass123")).toBe("Moderate");
});
test("testcase 6", () => {
    expect(passwordStrength("!@!pass1")).toBe("Moderate");
});
test("testcase 7", () => {
    expect(passwordStrength("@S3cur1ty")).toBe("Strong");
});