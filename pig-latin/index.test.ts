import pigLatin from ".";

test("testcase 1", () => {
    expect(pigLatin("this is pig latin")).toBe("isthay isway igpay atinlay");
});
test("testcase 2", () => {
    expect(pigLatin("wall street journal")).toBe("allway eetstray ournaljay");
});
test("testcase 3", () => {
    expect(pigLatin("raise the bridge")).toBe("aiseray ethay idgebray");
});
test("testcase 4", () => {
    expect(pigLatin("all pigs oink")).toBe("allway igspay oinkway");
});