export default function pigLatin(sentence : string) {
    const words = sentence.split(" ");
    let result : string[] = [];
    for (let word of words) {
        let firstVowel = word.toLowerCase().search(/[aeiou]/);
        if (firstVowel === 0) result.push(word + "way")
        else result.push(word.slice(firstVowel) + word.slice(0,firstVowel) + "ay");
    }
    return result.join(" ");
}