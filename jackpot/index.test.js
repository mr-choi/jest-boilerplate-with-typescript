import { jackpot } from ".";

test("testcase 1", () => {
    expect(jackpot(["@", "@", "@", "@"])).toBe(true);
});
test("testcase 2", () => {
    expect(jackpot(["abc", "abc", "abc", "abc"])).toBe(true);
});
test("testcase 3", () => {
    expect(jackpot(["SS", "SS", "SS", "SS"])).toBe(true);
});
test("testcase 4", () => {
    expect(jackpot(["&&", "&", "&&&", "&&&&"])).toBe(false);
});
test("testcase 5", () => {
    expect(jackpot(["SS", "SS", "SS", "Ss"])).toBe(false);
});
test("does not accept type coercion", () => {
    expect(jackpot([1, "1", "1", 1.0])).toBe(false);
});