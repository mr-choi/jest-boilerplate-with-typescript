export default function anagrams(word: string, wordList: string[]) {
    const letters = word.split("").sort().join("");
    return wordList.filter(value => {
        value = value.split("").sort().join("");
        return letters === value;
    });
}