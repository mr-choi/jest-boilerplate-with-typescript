import anagrams from ".";

test("testcase 1", () => {
   expect(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada'])).toEqual(["aabb", "bbaa"]); 
});
test("testcase 2", () => {
    expect(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer'])).toEqual(["carer","racer"]);
});
test("testcase 3", () => {
    expect(anagrams('laser', ['lazing', 'lazy', 'lacer'])).toEqual([]);
});