export default function rearrange(sentence:string) {
    const words = sentence.match(/\w+/g) || []; // Get array of the words
    const rearrangedWords = Array(words.length); // Already create the array.

    for (let word of words) {
        let index : number;
        let result = word.replace(/\d/, (match) => {
            index = parseInt(match)-1;
            return "";
        });
        rearrangedWords[index] = result;
    }
    return rearrangedWords.join(" ");
}