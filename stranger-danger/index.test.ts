import noStrangers from "."

test("correct order acquaintance", () => {
    expect(noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.")).toEqual([["spot","see"], []]);
});
test("friends are not in acquaintance", () => {
    expect(noStrangers("one and two and three and four and five, and finally six")).toEqual([[], ["and"]]);
});
test("covers apostrophes 1", () => {
    expect(noStrangers("don't, Don't. DON't")).toEqual([["don't"], []]);
});
test("covers apostrophes 2", () => {
    expect(noStrangers("don't, Don't. DON't doN't + don't don'T")).toEqual([[], ["don't"]]);
});
test("correct order friends", () => {
    expect(noStrangers("first, second, second, second, first, second, second, first, first, first")).toEqual([[],["second","first"]]);
});