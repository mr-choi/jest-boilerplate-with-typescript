export default function noStrangers(sentence:string) {
    const words = sentence.toLowerCase().match(/[a-z0-9']+/g);
    const occurrences = new Map();

    let acquaintances = []
    let friends = [];

    for (let word of words) {
        if (!occurrences.has(word)) occurrences.set(word, 0);
        occurrences.set(word, occurrences.get(word)+1);

        if (occurrences.get(word) === 3) {
            acquaintances.push(word);
        }
        if (occurrences.get(word) === 5) {
            friends.push(word);
            acquaintances = acquaintances.filter(w => w !== word);
        }
    }

    return [acquaintances, friends];
}