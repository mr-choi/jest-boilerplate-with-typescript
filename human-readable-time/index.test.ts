import humanReadableTime from ".";

test("testcase 1", () => {
    expect(humanReadableTime(0)).toBe("00:00:00");
});
test("testcase 2", () => {
    expect(humanReadableTime(59)).toBe("00:00:59");
});
test("testcase 3", () => {
    expect(humanReadableTime(60)).toBe("00:01:00");
});
test("testcase 4", () => {
    expect(humanReadableTime(90)).toBe("00:01:30");
});
test("testcase 5", () => {
    expect(humanReadableTime(3599)).toBe("00:59:59");
});
test("testcase 6", () => {
    expect(humanReadableTime(3600)).toBe("01:00:00");
});
test("testcase 7", () => {
    expect(humanReadableTime(45296)).toBe("12:34:56");
});
test("testcase 8", () => {
    expect(humanReadableTime(86399)).toBe("23:59:59");
});
test("testcase 9", () => {
    expect(humanReadableTime(86400)).toBe("24:00:00");
});
test("testcase 10", () => {
    expect(humanReadableTime(359999)).toBe("99:59:59");
});

/*
humanReadable(0) -> '00:00:00'
humanReadable(59) -> '00:00:59'
humanReadable(60) -> '00:01:00'
humanReadable(90) -> '00:01:30'
humanReadable(3599) -> '00:59:59'
humanReadable(3600) -> '01:00:00'
humanReadable(45296) -> '12:34:56'
humanReadable(86399) -> '23:59:59'
humanReadable(86400) -> '24:00:00'
humanReadable(359999) -> '99:59:59'
*/