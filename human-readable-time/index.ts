export default function humanReadableTime(time: number) {
    const seconds = time % 60;
    const minutes = Math.floor(time / 60) % 60;
    const hours = Math.floor(time / 3600);
    const secondsPrefix = (seconds < 10) ? "0" : "";
    const minutesPrefix = (minutes < 10) ? "0" : "";
    const hoursPrefix = (hours < 10) ? "0" : "";
    return hoursPrefix + hours + ":" + minutesPrefix + minutes + ":" + secondsPrefix + seconds;
}